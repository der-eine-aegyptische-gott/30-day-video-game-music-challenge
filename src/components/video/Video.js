import './Video.css'
import React from 'react'
import ReactPlayer from 'react-player/youtube'

function Video({ entry, winner }) {
  if (!entry.url) {
    return (
      <div className="pr-5 w-full md:w-4/12">
        <div className="aspect-w-16 aspect-h-9">
          <img src={`${process.env.PUBLIC_URL}/locked.jpg`} alt="Locked" />
        </div>
      </div>
    )
  }

  return (
    <div className="pr-5 w-full md:w-4/12">
      <div className="aspect-w-16 aspect-h-9">
        <ReactPlayer
          className={`react-player${winner ? ' winner' : ''}`}
          url={entry.url}
          width='100%'
          height='100%'
          light={true}
          controls={true}
        />
        {winner &&
          <svg xmlns="http://www.w3.org/2000/svg" className="winner-icon h-8 w-8" fill="none" viewBox="0 0 24 24"
               stroke="currentColor">
            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2}
                  d="M5 3v4M3 5h4M6 17v4m-2-2h4m5-16l2.286 6.857L21 12l-5.714 2.143L13 21l-2.286-6.857L5 12l5.714-2.143L13 3z"/>
          </svg>
        }
      </div>
      <div className="flex justify-between text-xl text-white m-2">
        <div>
          {entry.rating &&
            entry.rating.map((rating, index) => {
              return (
                <div key={index} className="flex">
                  <span className="capitalize">{rating.from}</span>:{' '}
                  {rating.points}/10
                </div>
              )
            })}
        </div>
        <div>
          Eingesendet von: <span className="capitalize">{entry.from}</span>
        </div>
      </div>
    </div>
  )
}

export default Video
