import RowHeader from './header/RowHeader'
import RowContent from './content/RowContent'

function Row({ data, index }) {
  return (
    <div className="mt-10 mb-10">
      <RowHeader title={data.title} day={index} />
      <RowContent
        entrys={data.entrys}
        honorableMentions={data.honorableMentions}
      />
    </div>
  )
}

export default Row
