import './RowHeader.css'
import { Link } from 'react-router-dom'

function RowHeader({ title, day }) {
  return (
    <div className="rowHeader mb-7">
      <h2>
        <span className="text-white text-xl">
          {title}
          <div className="honorableMention text-xl">
            {day && <Link to={`honorablementions/${day}`}>
              Honorable mentions
            </Link>}
          </div>
        </span>
      </h2>
    </div>
  )
}

export default RowHeader
