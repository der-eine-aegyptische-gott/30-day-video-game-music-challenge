import Video from '../../video/Video'
import checkIfEntryWon from "../../../helper/checkIfEntryWon";

function RowContent({ entrys }) {
  return (
    <div className="flex flex-row flex-wrap">
      {entrys.map((entry, index) => {
        return <Video key={index} entry={entry} winner={checkIfEntryWon(entrys,entry)}/>
      })}
    </div>
  )
}

export default RowContent
