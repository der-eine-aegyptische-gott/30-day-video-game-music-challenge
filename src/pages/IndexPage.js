import React from 'react'
import Row from '../components/row/Row'
import challengeData from '../data/challengeData.json'
import {Modal} from "../components/modal/Modal";
import Gitlab from './../gitlab-icon-rgb.png';

export const IndexPage = () => {
  const [showModal, setShowModal] = React.useState(false);
  const rows = challengeData.map((row, index) => (
    <Row key={index} data={row} index={index + 1} />
  ))
  function closeModal() {
    setShowModal(false)
  }
  return (
    <div className="m-10">
      <div className="text-3xl mb-5 title-challenge">
        <h1>30-Day Video Game Music Challenge</h1>
        <svg xmlns="http://www.w3.org/2000/svg" className="h-8 w-8" fill="none" viewBox="0 0 24 24" stroke="white" onClick={() => setShowModal(true)}>
          <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M12 8v4m0 4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
        </svg>
      </div>
      <div>{rows}</div>
      <div className={"footer"}>
        <div className={"gitlab"}>
          <a href={"https://gitlab.com/der-eine-aegyptische-gott/30-day-video-game-music-challenge"}><img src={Gitlab} alt={"Gitlab"}/></a>
        </div>
      </div>
      <Modal showModal={showModal} setShowModal={setShowModal} closeModal={closeModal}/>
    </div>
  )
}
