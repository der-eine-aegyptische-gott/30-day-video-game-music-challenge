import { useParams } from 'react-router'

import Video from '../components/video/Video'
import React from 'react'
import challengeData from '../data/challengeData.json'
import { Link } from 'react-router-dom'

export const HonorableMentionPage = () => {
  const { day } = useParams()
  const rows = challengeData[day - 1].honorableMentions.map((data, index) => (
    <Video key={index} entry={data} index={index + 1} />
  ))
  const rowTitle = challengeData[day - 1].title;
  return (
    <div className="m-10">
      <div className="flex text-white justify-between items-center">
        <div>
          <div>
            <div className="mb-5">
              <h1 className="text-3xl ">30-Day Video Game Music Challenge</h1>
            </div>
            <div className="mt-10 mb-7">
              <span className="text-xl">Honorable mentions - {rowTitle}</span>
            </div>
          </div>
        </div>
        <div className="mt-10">
          <Link className="text-xl" to={`/`}>Back to home</Link>
        </div>
      </div>
      <div className="flex">{rows}</div>
    </div>
  )
}
