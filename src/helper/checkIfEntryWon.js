const checkIfEntryWon = (entrys,entry) => {
  let tempMatrix = [];
  entrys.forEach((entry) => {
    let points = 0;
    if(entry.rating) {
      entry.rating.forEach((rating) => {
        points += rating.points;
      })
    }
    let tempObj = {
      id: entry.url,
      points
    };
    tempMatrix.push(tempObj);
  })
  tempMatrix.sort((a, b ) => b.points - a.points);
  return tempMatrix.find((ele) => ele.id === entry.url).points === tempMatrix[0].points;
}
export default checkIfEntryWon;
