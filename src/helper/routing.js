let URL;
if(process.env.REACT_APP_BASE_URL) {
  URL = `/${process.env.REACT_APP_BASE_URL}`;
} else {
  URL = '';
}
export default URL
